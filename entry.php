<?php
/*
 * WYF Framework
 * Copyright (c) 2011 James Ekow Abaka Ainooson
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

require "cfx_bootstrap.php";

$authExcludedPaths[] = "system/login";
$request = parse_url(filter_input(INPUT_SERVER, 'REQUEST_URI'));
$requestPath = Application::$selectedRoute = substr($request['path'], 1);

// Load the styleseets and the javascripts
// Bootstrap the application
$t = new TemplateEngine();
Application::$templateEngine = $t;

require SOFTWARE_HOME . "app/bootstrap.php";

$loggedIn = $_SESSION['logged_in'] ?? false;

// Authentication ... check if someone is already logged in if not force 
// a login
if (!$loggedIn && array_search($requestPath, $authExcludedPaths) === false && substr($requestPath, 0, 10) != "system/api") {
    $redirect = urlencode(Application::getLink("/$requestPath"));
    foreach (filter_input_array(INPUT_GET) ?? [] as $key => $value) {
        $redirect .= urlencode("$key=$value");
    }
    header("Location: " . Application::getLink("/system/login") . "?redirect=$redirect");
} else if ($loggedIn) {

    $t->assign('username', $_SESSION["user"]['user_name']);
    $t->assign('firstname', $_SESSION['user']['first_name']);
    $t->assign('lastname', $_SESSION['user']['last_name']);

    if (isset($_GET["notification"])) {
        $t->assign('notification', "<div id='notification'>" . $_GET["notification"] . "</div>");
    }
}



// Blast the HTML code to the browser!
Application::setSiteName(Application::$config['name']);
Application::render();
