<?php

/**
 * A Controller which validates a user and logs her in. This controller allows
 * first time users to create a new password when they log in.
 *
 * @author james
 *
 */
class SystemLoginController extends Controller
{
    /**
     * A method which allows the user to change their password if they are
     * logging in for the forst time.
     * @return unknown_type
     */
    public function change_password()
    {
        Application::$template = "login.tpl";
        Application::setTitle("Change Password");

        $form = new Form();
        $form->setRenderer("default");
        $password = new PasswordField("Password", "password");
        $password->setEncrypted(false);
        $form->add($password);

        $passwordRetype = new PasswordField("Retype Password", "password2");
        $passwordRetype->setEncrypted(false);
        $form->add($passwordRetype);
        $form->setValidatorCallback($this->getClassName() . "::change_password_callback");
        $form->setShowClear(false);
        $form = $form->render();

        return "<h2>Change Password</h2>"
            . "<p>It appears that this is the first time you are logging in. "
            . "Please change your password.</p> $form";
    }

    /**
     * The default page which shows the login form.
     * @see lib/controllers/Controller#getContents()
     */
    public function getContents()
    {
        Application::$template = "login.tpl";
        Application::setTitle("Login");

        if (key_exists("logged_in", $_SESSION)) {
            Application::redirect("/");
        }

        if (key_exists("token", $_GET) && Configuration::get("signup_token") == $_GET['token'] && time() < strtotime(Configuration::get("signup_token_expiry"))) {
            $user = Model::load("system.users");
            $userData = $user->get(["filter" => "user_id=?", 'bind' => [1]], Model::MODE_ASSOC, false, false);
            $_SESSION["logged_in"] = true;
            $_SESSION["user_id"] = $userData[0]["user_id"];
            $_SESSION['user'] = $userData[0];
            Application::redirect(Application::getLink("/"));
            return;
        }

        $form = new Form();
        $form->setRenderer("default");
        $username = new TextField("Username", "username");
        $form->add($username);
        $password = new PasswordField("Password", "password");
        $password->setEncrypted(false);
        $form->add($password);
        $form->setSubmitValue("Login");
        $form->setValidatorCallback("{$this->getClassName()}::callback");

        return $form->render();
    }


    /**
     * A callback function which checks the validity of passwords on the form.
     * It checks to ensure that the right user is logging in with the right
     * password.
     *
     * @param $data
     * @param $form
     * @param $callback_pass
     * @return unknown_type
     */
    public static function callback($data, $form, $callback_pass = null)
    {
        $user = Model::load("system.users");
        $userData = $user->get(
            array(
                "filter" => "user_name=?",
                'bind' => [$data["username"]]
            ), Model::MODE_ASSOC, false, false);

        if (count($userData) == 0) {
            $form->addError("Please check your username or password");
            return true;
        } else if ($userData[0]["role_id"] == null) {
            $form->addError("Sorry! your account has no role attached!");
            return true;
        } else  {
            $home = Application::getLink("/");

            /* Verify the password of the user or check if the user is logging in
             * for the first time.
             */
            if (password_verify($data['password'], $userData[0]["password"]) && $userData[0]["user_status"] == 1) {
                $_SESSION["logged_in"] = true;
                $_SESSION["user_id"] = $userData[0]["user_id"];
                $_SESSION['user'] = $userData[0];
                Application::redirect($home);
            } else {
                $form->addError("Please check your username or password");
                return true;
            }
        }
    }
}
