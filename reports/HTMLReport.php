<?php

class HTMLReport extends Report
{
    private $numColumns;
    private $widthsSet;

    public function output($file=null)
    {
        foreach ($this->contents as $content) {

            if (!is_object($content)) continue;
            switch ($content->getType()) {

                case "text":
                    $style = "padding:0px;margin:0px;";
                    if (isset($content->style["font"])) $style .= "font-family:{$content->style["font"]};"; else $style .= "font-family:inherit;";
                    if (isset($content->style["size"])) $style .= "font-size:{$content->style["size"]}pt;";
                    if (isset($content->style["top_margin"])) $style .= "margin-top:{$content->style["top_margin"]}px;";
                    if (isset($content->style["bottom_margin"])) $style .= "margin-bottom:{$content->style["bottom_margin"]}px;";

                    $style .= $content->style["bold"] ? "font-weight:bold;" : "";
                    $style .= $content->style["underline"] ? "text-decoration:underline;" : "";
                    $style .= $content->style["align"] == 'R' ? "text-align:right" : "";

                    print "<div style='$style'>" . $content->getText() . "</div>";
                    break;

                case "table":
                    $headerFill = implode(',', $content->style['header:background']);
                    $headerBorder = implode(',', $content->style['header:border']);
                    $headerText = implode(',', $content->style['header:text']);
                    $bodyFill = implode(',', $content->style['body:background']);
                    $bodyStripe = implode(',', $content->style['body:stripe']);
                    $bodyBorder = implode(',', $content->style['body:border']);
                    $bodyText = implode(',', $content->style['body:text']);

                    if ($content->style["totalsBox"]) {
                        $totals = $content->getData();
                        print "<tr class='totals-box'>";
                        for ($i = 0; $i < $this->numColumns; $i++) {
                            if ($i == 0) {
                                print "<td>{$totals[$i]}</td>";
                            } else {
                                print "<td>" . (is_numeric($totals[$i]) ? number_format($totals[$i], 2, '.', ',') : "") . "</td>";
                            }
                        }
                        print "</tr>";
                    } else {
                        $totalWidths = array_sum($content->data_params["widths"]);
                        foreach ($content->data_params["widths"] as $i => $width) {
                            $this->widths[$i] = round($width / $totalWidths * 100);
                        }
                        print "<table class='report-table'><thead><tr>";
                        $tableOpen = true;
                        $headers = $content->getHeaders();
                        $this->numColumns = count($headers);
                        foreach ($headers as $key => $header) {
                            $number = $content->data_params["type"][$key] == 'number' || $content->data_params["type"][$key] == 'double' ? 'number' : '';
                            print "<td class='$number'>{$headers[$key]}</td>";
                        }
                        print "</tr></thead><tbody>";
                        $fill = false;
                        $data = $content->getData();
                        foreach ($data as $row) {
                            print "<tr>";
                            $keys = array_keys($row);
                            foreach ($headers as $i => $header) {
                                $key = $keys[$i];
                                $number = $content->data_params["type"][$i] == 'number' || $content->data_params["type"][$i] == 'double' ? 'number' : '';
                                $value = $number ? number_format($row[$key], 2) : $row[$key];
                                print "<td class='$number'>{$value}</td>";
                            }
                            print "</tr>";
                            $fill = !$fill;
                        }
                        print "</tbody><tfoot>";
                        if ($content->style["autoTotalsBox"]) {
                            $totals = $content->getTotals();
                            print "<tr class='totals-box'>";
                            foreach ($headers as $index => $header) {
                                if ($index == 0) {
                                    print "<td>Totals</td>";
                                } else {
                                    $number = $content->data_params["type"][$index] == 'number' || $content->data_params["type"][$index] == 'double' ? 'number' : '';
                                    $value = is_numeric($totals[$index]) ? number_format($totals[$index], 2) : "";
                                    print "<td class='$number'>{$value}</td>";
                                }
                            }
                            print "</tr>";
                        }
                        print "</tfoot></table>";
                    }
                    break;
            }
        }
        die();
    }
}
