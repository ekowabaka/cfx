<?php

use ntentan\AbstractMiddleware;

class CfxNtentanMiddleware extends AbstractMiddleware
{
    private $config;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function run($route, $response)
    {
        define("SOFTWARE_HOME", $this->config['home']);

        // Setup the database driver and other boilerplate stuff
        $dbDriver = $this->config['db'][$this->config['selected']]['driver'];
        $dbDriverClass = Application::camelize($dbDriver);

        Db::$defaultDatabase = $this->config['selected'];
        SQLDBDataStore::$activeDriverClass = $dbDriverClass;

        //$this->config['db'][$this->config['selected']]['name'] = 'company_' . Session::get('company_details')['id'];
        Application::$config = $this->config;
        Cache::init($this->config['cache']['method']);
        define('CACHE_MODELS', $this->config['cache']['models']);
        define('CACHE_PREFIX', "");
        define('ENABLE_AUDIT_TRAILS', $this->config['audit_trails']);

        $requestPath = Application::$selectedRoute = $route['route'];

        // Bootstrap the application
        $t = new TemplateEngine();
        Application::$templateEngine = $t;
        require "app/bootstrap.php";


        Application::setSiteName(Application::$config['name']);
        Application::render();
    }
}