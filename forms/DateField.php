<?php

/**
 * A special field for accepting dates from the user. Although this field uses a javascript
 * pop-up to allow a much more interactive way for capturing dates from the
 * user, users can also type in their dates. If dates are entered in a wrong
 * format, an internal validation ensures that the user is prompted. The date
 * field can optionally capture and display time information along with the
 * date. The format accepted by this field is 'd/m/y h:m:s'.
 *
 * @author James Ekow Abaka Ainooson <jainooson@gmail.com>
 * @ingroup Forms
 */
class DateField extends TextField
{
    public function render()
    {
        $this->addAttribute("type", "date");
        return parent::render();
    }
}
