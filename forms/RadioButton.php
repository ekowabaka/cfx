<?php

/**
 * A standard radio button. Can be added to the radio button group.
 * @ingroup Forms
 */
class RadioButton extends Field
{
    private $checkedValue;

    /**
     * The constructor of the radio button.
     *
     * @param $label
     * @param $value
     * @param $description
     * @param $id
     */
    public function __construct($label="", $value="")
    {
        parent::__construct();
        $this->setLabel($label);
        $this->checkedValue = $value;
    }

    public function getCheckedValue()
    {
        return $this->checkedValue;
    }

    public function setCheckedValue($checkedValue)
    {
        $this->checkedValue = $checkedValue;
    }

    public function render()
    {
        return "<label><input class='fapi-radiobutton {$this->getCSSClasses()}' {$this->getAttributes()} type='radio' name='{$this->getName()}' value='{$this->getCheckedValue()}' ".($this->getValue()==$this->getCheckedValue()?"checked='checked'":"")."/>{$this->getLabel()}</label>";
    }
}

