<?php

/**
 * The Form class is the main Container for all forms. This form class renders
 * HTML forms. This class is mostly extended to create new forms. In some rare
 * cases it is directly used and the other
 *
 * @ingroup Forms
 * @see Container::add
 */
class Form extends Container
{
    protected $ajaxSubmit;

    /**
     * The value to be printed on the submit form.
     */
    protected $submitValue;

    /**
     * Flag to show wether this form has a reset button.
     */
    protected $showClear = false;

    /**
     * The value to display on the reset button.
     */
    protected $resetValue;

    /**
     * A boolean value which shows whether to show the submit button or not.
     * @var boolean
     */
    protected $showSubmit = true;

    /**
     * Attributes attached to the submit attributes.
     * @var Attribute
     */
    public $submitAttributes;

    public $ajaxAction = "lib/fapi/ajax.php?action=save_data";

    public $successUrl;

    /**
     * A constructor for initializing the form. This constructor should always
     * be called even when the form class is being extended.
     * @param $method
     */
    public function __construct($method = "")
    {
        parent::__construct();

        if ($method == "") $method = "POST";
        $this->setMethod($method);
        $this->ajax = true;
        $this->setSubmitValue("Save");
        $this->addAttribute("class", "fapi-form form-grid");
    }

    /**
     * Renders the form. This method is called from the Form::render() method.
     */
    protected function renderForm()
    {
        $this->addAttribute("method", $this->getMethod());

        if ($this->getHasFile()) $this->addAttribute("enctype", "multipart/form-data");

        $ret = '<form ' . $this->getAttributes() . '>';
        if ($this->getHasFile()) {
            $ret .= "<input type='hidden' name='MAX_FILE_SIZE' value='10485760' />";
        }
        $ret .= $this->renderElements();

        $onclickFunction = "fapi_ajax_submit_" . $this->getId() . "()";
        $onclickFunction = str_replace("-", "_", $onclickFunction);

        if ($this->getShowSubmit()) {
            $ret .= '<div class="form-submit-area">';
            $submitValue = $this->submitValue ? ('value="' . $this->submitValue . '"') : "";
            $ret .= sprintf('<input class="fapi-submit" type="submit" %s />', $submitValue);
            $ret .= '</div>';
        }
        $id = $this->getId();

        if ($id != '') {
            $ret .= "<input type='hidden' name='is_form_{$id}_sent' value='yes' />";
        }
        $ret .= '<input type="hidden" name="is_form_sent" value="yes" />';

        $ret .= '</form>';

        if ($this->ajaxSubmit) {
            $elements = $this->getFields();
            $ajaxData = array();
            foreach ($elements as $element) {
                $id = $element->getId();
                if ($element->getStorable()) {
                    $ajaxData[] = "'" . urlencode($id) . "='+document.getElementById('$id')." . ($element->getType() == "Field" ? "value" : "checked");
                }
                $validations = $element->getJsValidations();
                $validators .= "if(!fapiValidate('$id',$validations)) error = true;\n";
            }
            $ajaxData[] = "'fapi_dt=" . urlencode($this->getDatabaseTable()) . "'";
            $ajaxData = addcslashes(implode("+'&'+", $ajaxData), "\\");

            $ret .=
                "<script type='text/javascript'>
            function $onclickFunction
            {
                var error = false;
                $validators
                if(error == false)
                {
                    \$.ajax({
                        type : 'POST',
                        url : '{$this->ajaxAction}',
                        data : $ajaxData
                    });
                }
            }
            </script>";
        }
        return $ret;
    }

    public function submittedCallback()
    {
        if ($this->isFormSent()) {
            $data = $this->getData();
            $validated = $this->validatorCallback == "" ? 1 : $this->executeCallback($this->validatorCallback, $data, $this, $this->validatorCallbackData);
            if ($validated == 1) {
                $this->executeCallback($this->callback, $data, $this, $this->callbackData);
            }
        }
    }

    public function render()
    {
        $this->onRender();
        $this->submittedCallback();
        return $this->renderForm();
    }

    /**
     * Sets the value which is displayed on the submit button.
     */
    public function setSubmitValue($submitValue)
    {
        $this->submitValue = $submitValue;
        return $this;
    }

    /**
     * Setter method for the showSubmit parameter.
     * @param boolean $showSubmit
     */
    public function setShowSubmit($showSubmit)
    {
        $this->showSubmit = $showSubmit;
        return $this;
    }

    /**
     * Getter method for the showSubmit parameter.
     */
    public function getShowSubmit()
    {
        return $this->showSubmit;
    }

    public function setShowClear($showClear)
    {
        $this->showClear = $showClear;
    }

    public function getShowClear()
    {
        return $this->showClear;
    }

    public function setShowField($show_field)
    {
        Container::setShowField($show_field);
        $this->setShowSubmit($show_field);
        return $this;
    }

    public function useAjax($validation = true, $submit = true)
    {
        $this->ajax = $validation;
        $this->ajaxSubmit = $submit;
    }
}
