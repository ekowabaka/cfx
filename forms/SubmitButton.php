<?php

class SubmitButton extends Button
{
    public function __construct($label="", $value = "")
    {
        parent::__construct($label);
    }

    public function render()
    {
        $this->setAttribute("type","submit");
        return parent::render();
    }
}