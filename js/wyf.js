/**
 * Main package for the entire WYF javascript file.
 */

var wyf =
    {
        getMulti: function (params, callback) {
            $.getJSON('/system/api/get_multi?params=' + escape(JSON.stringify(params)),
                function (response) {
                    if (typeof callback === 'function') callback(response);
                }
            );
        },

        openWindow: function (location) {
            window.open(location);
        },

        showUploadedData: function (data) {
            $("#import-preview").html(data);
        },

        updateFilter: function (table, model, value) {
            if (value == 0) {
                externalConditions[table] = "";
            } else {
                externalConditions[table] = model + "= ?";
                externalBoundData = [value];
            }
            window[table + 'Search']();
        },

        confirmRedirect: function (message, path) {
            if (confirm(message)) {
                document.location = path;
            }
        },

        init: function () {
            wyf.menus.init();
            wyf.tapi.init();
        },

        menus:
            {
                active: null,
                expand: function (id) {
                    $("#" + wyf.menus.active).slideUp("fast");
                    $("#" + id).slideDown("fast",
                        function () {
                            wyf.menus.active = id;
                        }
                    );
                },

                init: function () {
                    var menuObject = $('#main-menu');
                    var mainHeader = $('#main-header');
                    menuObject.height($(window).height() - mainHeader.height());
                    $(window).resize(function(event) {
                        menuObject.height($(window).height() - mainHeader.height());
                    })
                }
            },

        tapi:
            {
                tables: new Object(),
                tableIds: new Array(),
                activity: null,

                addTable: function (id, obj) {
                    wyf.tapi.tableIds.push(id);
                    wyf.tapi.tables[id] = obj;
                    wyf.tapi.tables[id].prevPage = 0;
                },

                init: function () {
                    for (var i = 0; i < wyf.tapi.tableIds.length; i++) {
                        var id = wyf.tapi.tableIds[i];
                        //$("#"+id+">tbody").load(wyf.tapi.tables[id].path);
                        wyf.tapi.render(wyf.tapi.tables[id]);
                    }
                },

                render: function (table, action, params) {
                    var urlParams = "params=" + escape(JSON.stringify(table));

                    try {
                        wyf.tapi.activity.abort();
                    } catch (e) {

                    }

                    wyf.tapi.activity = $.ajax({
                        type: "POST",
                        url: table.url,
                        dataType: "json",
                        data: urlParams,
                        success: function (r) {
                            if(r.tbody == '') {
                                $('#empty-table').show();
                                $('.tapi-table').hide();
                                $('.table-footer').hide();
                            } else {
                                $('#empty-table').hide();
                                $('.tapi-table').show();
                                $('.table-footer').show();
                                $("#" + table.id + ">tbody").html(r.tbody);
                            }
                        }
                    });
                },

                sort: function (id, field) {
                    if (wyf.tapi.tables[id].sort == "ASC") {
                        wyf.tapi.tables[id].sort = "DESC";
                    } else {
                        wyf.tapi.tables[id].sort = "ASC";
                    }

                    wyf.tapi.tables[id].sort_field[0].field = field;
                    wyf.tapi.tables[id].sort_field[0].type = wyf.tapi.tables[id].sort;
                    wyf.tapi.render(wyf.tapi.tables[id]);
                },

                switchPage: function (id, page) {
                    var table = wyf.tapi.tables[id];
                    table.page = page;
                    wyf.tapi.render(table);
                    $("#" + id + "-page-id-" + page).addClass("page-selected");
                    $("#" + id + "-page-id-" + table.prevPage).removeClass("page-selected");
                    table.prevPage = page;
                },

                showSearchArea: function (id) {
                    $("#tapi-" + id + "-search").toggle();
                }
            }
    };

function expand(id) {
    $("#" + id).slideToggle("fast",
        function () {
            document.cookie = id + "=" + $("#" + id).css("display");
            if (typeof menuExpanded === 'function') {
                menuExpanded();
            }
        }
    );
}
