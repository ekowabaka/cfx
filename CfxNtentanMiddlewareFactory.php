<?php

use ntentan\config\Config;
use ntentan\interfaces\MiddlewareFactoryInterface;

class CfxNtentanMiddlewareFactory implements MiddlewareFactoryInterface
{
    private $config;

    public function __construct(Config $config)
    {
        $this->config = $config->get('main');
    }

    public function createMiddleware(array $parameters): \ntentan\AbstractMiddleware
    {
        return new CfxNtentanMiddleware($this->config);
    }
}