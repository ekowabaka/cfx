<?php

class MultiModelTable extends Table
{
    private $fields = array();
    private $itemsPerPage = 15;
    public $useAjax = false;
    protected $params;
    protected $tableData;
    protected $model;
    private $searchScript;

    public function __construct($prefix = null)
    {
        parent::__construct($prefix);
    }

    public function setParams($params)
    {
        $params["limit"] = $this->itemsPerPage;
        $params["offset"] = $this->itemsPerPage * $params["page"];
        $params["url"] = Application::$prefix . "/system/api/table";
        $params["id"] = $this->name;
        $params["operations"] = $this->operations;
        $params["moreInfo"] = true;

        $this->params = $params;

        if ($this->useAjax) {
            $params['moreInfoOnly'] = true;
        }

        $this->tableData = SQLDBDataStore::getMulti($params);
    }

    protected function renderHeader()
    {
        $searchFunction = $this->name . "Search()";
        $table = "<table class='tapi-table' id='$this->name'>";

        //Render Headers
        $table .= "<thead><tr>";
        foreach ($this->headers as $i => $header) {
            $table .= "<td onclick=\"wyf.tapi.sort('" . $this->name . "','" . $this->tableData["rawFields"][$i + 1] . "')\">
            $header
            </td>";
        }

        $table .= "<td style='align:right' class='operations'>Operations</td>";
        $table .= "</tr>";

        //Render search fields
        $table .= "<tr id='tapi-search-row' >";

        foreach ($this->headers as $i => $header) {
            $table .= "<td>";
            switch ($this->fields[$i + 1]["type"]) {
                case "string":
                case "text":
                    $text = new TextField();
                    $text->setId($this->fields[$i + 1]["name"]);
                    $text->addAttribute("onkeyup", $searchFunction);
                    $table .= $text->render();
                    $name = $this->fields[$i + 1]["name"];
                    $this->searchScript .= "if($('#$name').val()!='') { conditions = (conditions==''?'':conditions+' AND ')+ \"lower({$this->tableData["rawFields"][$i+1]}::varchar) like ?\"; boundData.push('%'+$('#$name').val().toLowerCase()+'%');}\n";
                    break;

            }
            $table .= "</td>";
        }

        $table .= "<td></td></tr></thead>";

        //Render Data
        $table .= "<tbody id='tbody'>";

        return $table;
    }

    public function render($headers = true)
    {
        global $redirectedPackage;
        $results = $this->tableData;
        $this->fields = $results["fieldInfos"];

        foreach ($this->fields as $field) {
            if ($field["type"] == "number" || $field["type"] == "double" || $field["type"] == "integer") {
                $this->headerParams[$field["name"]]["type"] = "number";
            }
        }

        $this->headers = $results["headers"];
        array_shift($this->headers);
        if ($headers === true) $table = $this->renderHeader();
        if ($this->useAjax) {
            $table .= "<tr>
                <td align='center' colspan='" . count($this->headers) . "'>
                    <img style='margin:80px' src='/" . Application::getLink(Application::getWyfHome("tapi/images/loading-image-big.gif")) . "' />
                </td></tr>";
        } else {
            $this->data = $results["data"];
            $table .= parent::render(false);
        }

        if ($headers === true) $table .= $this->renderFooter();

        if ($this->useAjax) {
            $this->params['redirected_package'] = $redirectedPackage;
            $table .=
                "
            <script type='text/javascript'>
                wyf.tapi.addTable('$this->name',(" . json_encode($this->params) . "));
                var externalConditions = [];
                var externalBoundData = [];
                function {$this->name}Search()
                {
                    var conditions = '';
                    var boundData = [];
                    {$this->searchScript}
                    wyf.tapi.tables['$this->name'].filter = conditions;
                    wyf.tapi.tables['$this->name'].bind = boundData;
                    if(externalConditions['$this->name'])
                    {
                        wyf.tapi.tables['$this->name'].filter += ((conditions != '' ?' AND ':'') + externalConditions['$this->name']);
                        wyf.tapi.tables['$this->name'].bind = boundData.concat(externalBoundData);
                    }
                    wyf.tapi.tables['$this->name'].page = 0;
                    wyf.tapi.render(wyf.tapi.tables['$this->name']);
                }
            </script>";
        }
        return $table;
    }

    public function renderFooter()
    {
        $table = parent::renderFooter();
        $params = $this->params;

        $table .= "<div class='table-footer'>
            <ul class='table-pages'><li>
                        <a onclick=\"wyf.tapi.switchPage('$this->name',0)\">
                            <i class='fa fa-fast-backward'></i> First
                        </a>
                    </li>
                    <li>
                        <a onclick=\"wyf.tapi.switchPage('$this->name'," . ($params["page"] - 1 >= 0 ? $params["page"] - 1 : "") . ")\">
                            <i class='fa fa-backward'></i> Prev
                        </a>
                    </li>" .
            "<li><a onclick=\"wyf.tapi.switchPage('$this->name'," . ($params["page"] + 1) . ")\">Next <i class='fa fa-forward'></i></a></li>" .
            "<li> &bullet; </li>
            <li> Page <input style='font-size:small; width:50px' value = '" . ($params["page"] + 1) . "' onchange=\"wyf.tapi.switchPage('$this->name',(this.value > 0 )?this.value-1:0)\" type='text' /></li>
            </ul>
        </div>";
        return $table;
    }
}
