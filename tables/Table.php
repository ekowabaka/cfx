<?php

class Table
{
    protected $data = array();
    protected $headers = array();
    protected $cellRenderers = array();
    protected $operations = array();
    protected $prefix;
    public $name = "defaultTable";
    public $headerParams;

    public function __construct($prefix, $headers = null, $data = null, $operations = null, $headerParams = null)
    {
        $this->prefix = $prefix;
        $this->headers = $headers;
        $this->data = $data;
    }

    public function setOperations($operations)
    {
        $this->operations = $operations;
    }

    public function addOperation($link, $label = null, $action = null)
    {
        $this->operations[] =
            array
            (
                "link" => $link,
                "label" => $label ?? $label,
                "action" => $action ?? $this->prefix . $link . "/%key%"
            );
    }

    public function getOperations()
    {
        return $this->renderedOperations;
    }

    protected function renderHeader()
    {
        $table = "<table class='tapi-table'>";

        //Render Headers
        $table .= "<thead><tr>";
        $table .= implode("</td><td>", $this->headers);
        $table .= "</td><td>Operations</td></tr></thead>";

        //Render Data
        $table .= "<tbody id='tbody'>";
        return $table;
    }

    protected function renderFooter()
    {
        return "</tbody></table>";
    }

    public function render($renderHeaders = true)
    {
        if ($renderHeaders) $table = $this->renderHeader();

        foreach ($this->data as $i => $row) {
            $key = array_shift($row);
            $table .= "<tr>";

            foreach ($row as $name => $value) {
                $params = "";
                if ($this->headerParams[$name]["type"] == "number") {
                    $params = "align='right'";
                }
                $table .= "<td $params >" . strip_tags($value) . "</td>";
            }

            if ($this->operations != null) {
                $rowOperations = '';
                foreach ($this->operations as $operation) {
                    $rowOperations .= sprintf(
                        '<a class="fa operation-link operation-%s" href="%s" title="%s"></a>',
                        $operation['link'],
                        str_replace('%key%', $key, $operation['action']),
                        $operation['label']
                    );
                }
            }

            $table .= "<td id='{$this->name}-operations-cell-$i' align='right' >$rowOperations</td>";
            $table .= "</tr>";
        }

        if ($renderHeaders) $table .= $this->renderFooter();

        return "$table";
    }
}

